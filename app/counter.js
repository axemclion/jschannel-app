import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Button
} from 'react-native';

export default class jschannelapp extends Component {
  state = { count: 0 }
  add(val) {
    this.changeCounter(val);
  }
  subtract(val) {
    this.changeCounter(-val);
  }

  changeCounter(val) {
    this.setState(({ count }) => ({ count: count + val }));
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Counter = {this.state.count}
        </Text>
        <Button title="Add One" onPress={() => this.add(1)} />
        <Button title="Subtract One" onPress={() => this.subtract(1)} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
