import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    ListView
} from 'react-native';

import Button from './Button'

import Ionicons from 'react-native-vector-icons/Ionicons';
import Metadata from './../../package.json';

export default class Drawer extends Component {
    render() {
        return (<View style={styles.container}>
            <View style={styles.logoContainer}>
                <Ionicons size={150} name='ios-basket-outline' color='#333' />
                <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                    <Text style={{ fontSize: 30, fontWeight: 'bold' }}>{Metadata.name} </Text>
                    <Text>{Metadata.version}</Text>
                </View>
                <Text style={{ fontSize: 16, margin: 15, color: '#333' }}>{Metadata.description}</Text>
            </View >
            <View style={{ flex: 1 }}>
            </View>
        </View >);
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logoContainer: {
        alignItems: 'center',
        backgroundColor: '#ddd',
        marginBottom: 20,
        paddingTop: 20
    }
});