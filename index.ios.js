import React from 'react';
import {AppRegistry} from 'react-native';

import App from './app/counter.js';

AppRegistry.registerComponent('jschannelapp', () => App);
